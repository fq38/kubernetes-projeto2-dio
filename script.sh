#!/bin/bash

kubectl apply -f ./mysql-deployment.yaml --record
kubectl apply -f ./app-deployment.yaml --record
kubectl apply -f ./load-balancer.yml --record
kubectl apply -f ./secrets.yml --record


